import HomePage from "../POM/HomePage";
import BugPopUp from "../POM/BugPopUp";
describe('UI Automation | purchase Journey', () => {
    beforeEach(() => {
        cy.viewport(1280, 1000)
        cy.visit(Cypress.env("baseUrl"))
        cy.wait(2000);
        //handle cookies pop up
        HomePage.CloseCookiesPopUp();
    })
    it('Verify product add to cart journey', () => {
        //add product to cart
        HomePage.AddProductwithIndex(2);

        cy.get(HomePage.AddedCartCTA).should('be.visible').then(() => {
            cy.get(HomePage.Cart).should('be.visible').then((el) => {
                cy.get(el).click({ force: true })
            })
        })
        /// Verifying the Bucket price 
        let price = []
        cy.get(HomePage.CartSubTotal).then((el) => {
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        cy.get(HomePage.CartShippingTax).then((el) => {
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        cy.get(HomePage.CartTotal).then((el) => {
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        let totalPrice = Number(price[2]);
        let SubTotal = Number(price[0]);
        let Tax = Number(price[1]);
        if (totalPrice == SubTotal + Tax) {
            cy.log("SubTotal and tax is matching with Total price")
        } else {
            cy.log("SubTotal and tax is not matching with Total price")
        }
        //
    })
    it('verify the product quantity update feature', () => {
        //add product to cart
        HomePage.AddProductwithIndex(2);

        cy.get(HomePage.AddedCartCTA).should('be.visible').then(() => {
            cy.get(HomePage.Cart).should('be.visible').then((el) => {
                cy.get(el).click({ force: true })
            })
        })
        // Increase Product count 
        cy.get(HomePage.ProductQuantity).invoke('attr', 'value').then((value) => {
            expect(Number(value)).to.eq(1)

        })
        cy.get(HomePage.ProudctCountIncrease).click({ force: true });
        //update the new count
        cy.get(HomePage.ProudctCountUpdate).eq(0).click();
        // bug finder pop up is opened
        cy.wait(5000);
        cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
            cy.log("bug Triggered")
            //closing the bug pop up
            cy.get(BugPopUp.CloseBug).eq(-1).click({ force: true })
            cy.wait(2000);
            cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
                cy.get(BugPopUp.CloseBug).eq(1).click({ force: true })
            })
        })

    })
    it('verify the product currency update feature', () => {
        //add product to cart
        HomePage.AddProductwithIndex(2);

        cy.get(HomePage.AddedCartCTA).should('be.visible').then(() => {
            cy.get(HomePage.Cart).should('be.visible').then((el) => {
                cy.get(el).click({ force: true })
            })
        })
        // Change the Currency of Product
        cy.get(HomePage.CartCurrency).select(1)
        // bug finder pop up is opened
        cy.wait(7000);
        cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
            cy.log("bug Triggered")
            //closing the bug pop up
            cy.get(BugPopUp.CloseBug).eq(-1).click({ force: true })
            cy.wait(2000);
            cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
                cy.get(BugPopUp.CloseBug).eq(1).click({ force: true })
            })
        })

    })
    it('verify the empty product cart feature', () => {
        //add product to cart
        HomePage.AddProductwithIndex(2);

        cy.get(HomePage.AddedCartCTA).should('be.visible').then(() => {
            cy.get(HomePage.Cart).should('be.visible').then((el) => {
                cy.get(el).click({ force: true })
            })
        })
        //update the new count
        cy.get(HomePage.CartItemDelete).click({ force: true })
        cy.wait(2000);
        cy.get(HomePage.CartEmptyCTA).should('be.visible').then(() => {
            cy.get(HomePage.CartEmptyCTA).click({ force: true })
        })
        // bug finder pop up is opened
        cy.wait(3000);
        cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
            cy.log("bug Triggered")
            //closing the bug pop up
            cy.get(BugPopUp.CloseBug).eq(-1).click({ force: true })
            cy.wait(2000);
            cy.get(BugPopUp.BugPOpUps).scrollIntoView().then(() => {
                cy.get(BugPopUp.CloseBug).eq(1).click({ force: true })
            })
        })

    })
})