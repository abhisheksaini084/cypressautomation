import FindBug from "../POM/HomePage"
describe('UI Automation | purchase Journey', () => {
    
    it('Verify product add to cart journey', () => {
        cy.visit('https://academybugs.com/find-bugs/');
        cy.wait(2000);
        //handle cookies pop up
        cy.get('.cc-dismiss').click({force:true})
        //add product to cart
       cy.get('[id ^="ec_product_li_"] >div >div > span > [id ^="ec_add_to_cart"]').eq(2).click({force:true});
       cy.get('.ec_product_added_to_cart').should('be.visible').then(()=>{
        cy.get('.ec_product_added_to_cart > a').should('be.visible').then((el)=>{
            cy.get(el).click({force:true})
        })
       })
    /// Verifying the Bucket price 
        let price=[]
        cy.get('#ec_cart_subtotal').then((el)=>{
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        cy.get('#ec_cart_shipping').then((el)=>{
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        cy.get('#ec_cart_total').then((el)=>{
            let price1 = el.text().split('$')[1]
            price.push(price1)
        })
        let totalPrice = Number(price[2]);
        let SubTotal = Number(price[0]);
        let Tax = Number(price[1]);
        if(totalPrice == SubTotal+Tax){
            cy.log("SubTotal and tax is matching with Total price")
        }else{
            cy.log("SubTotal and tax is not matching with Total price")
        }
        //
    })
})