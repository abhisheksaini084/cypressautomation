class HomePage {
    Product = '[id ^="ec_product_li_"]'
    ProductAddCart = '[id ^="ec_product_li_"] >div > #ec_add_to_cart'
    AddProductToCart = '[id ^="ec_product_li_"] >div >div > span > [id ^="ec_add_to_cart"]'
    AddedCartCTA = '.ec_product_added_to_cart'
    Cart = '.ec_product_added_to_cart > a'
    CartSubTotal = '#ec_cart_subtotal'
    CartShippingTax = '#ec_cart_shipping'
    CartTotal = '#ec_cart_total'
    ProductQuantity = '.ec_quantity'
    ProudctCountIncrease = '.ec_plus'
    ProudctCountUpdate = '.ec_cartitem_update_button'
    CartCurrency = '#ec_currency_conversion'
    CartItemDelete = '.ec_cartitem_delete'
    CartEmptyCTA ='.ec_cart_empty_button'
    CookiesClose = '.cc-dismiss'
    CallAddToCart(index) {
        cy.get(this.ProductAddCart).eq(index).click({ force: true })
    }
    AddProductwithIndex(index) {
        cy.get(this.AddProductToCart).eq(index).click({ force: true });
    }
    CloseCookiesPopUp(){
        cy.get(this.CookiesClose).click({force:true});
    }
}
export default new HomePage();