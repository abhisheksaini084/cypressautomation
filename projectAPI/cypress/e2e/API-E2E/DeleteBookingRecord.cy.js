describe('API Automation | Deleting the Booking ID of User', () => {
    before(() => {
        cy.request({
            method: 'POST',
            url: 'https://restful-booker.herokuapp.com/auth',
            headers: {
                "Content-Type": "application/json",
            }, 
            body:{
                username: "admin",
                password: "password123"
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                expect(response.body.token).to.not.be.null
                cy.writeFile('cypress/fixtures/token.json', { token:  response.body.token})  
            }else{
                cy.writeFile('cypress/fixtures/token.json', { token: "" })
            }
        })  
    })
    it('Verify the Deletion of Booking record ', () => {
        cy.fixture('token.json').then((Admin) => {
        cy.readFile('cypress/fixtures/User.json').then((user) => {
            let token = Admin.token;
            let Bookingurl = `https://restful-booker.herokuapp.com/booking/${user.bookingid}`
            cy.request({
                method: 'DELETE',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                    "Cookie" : `token=${token}`
                },
              
            }).then((response) => {
                if (response.status == 201) {
                    cy.log(JSON.stringify(response.body))
                    if(response.body == 'Created'){
                        cy.log("user is deleted successfully")
                    }
                    
                }
            })
        })
    })
    })
    
})