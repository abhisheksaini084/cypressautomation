describe('API Automation | user Creation and filter verification', () => {
    it('Verify the creation of new Booking', () => {
        cy.readFile('cypress/fixtures/newUser.json').then((user) => {
            cy.request({
                method: 'POST',
                url: 'https://restful-booker.herokuapp.com/booking',
                headers: {
                    "Content-Type": "application/json",
                },
                body: user
            }).then((response) => {
                if (response.status == 200) {
                    cy.log(JSON.stringify(response.body))
                    let arr = response.body;
                    cy.writeFile('cypress/fixtures/User.json', arr)
                }
            })
        })
    })
    it('Verify the User with Booking ID & assert all data', () => {
        cy.fixture('User.json').then((user) => {
            cy.log(user.id)
            let Bookingurl = `https://restful-booker.herokuapp.com/booking/${user.bookingid}`
            cy.request({
                method: 'GET',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                }
            }).then((response) => {
                if (response.status == 200) {
                    let arr = response.body;
                    if (expect(arr.firstname).not.to.be.null) {
                        expect(arr.firstname).to.eq(user.booking.firstname)
                    }
                    if (expect(arr.lastname).not.to.be.null) {
                        expect(arr.lastname).to.eq(user.booking.lastname)
                    }
                    if (expect(arr.totalprice).not.to.be.null) {
                        expect(arr.totalprice).to.eq(user.booking.totalprice)
                    }
                    if (expect(arr.depositpaid).not.to.be.null) {
                        expect(arr.depositpaid).to.eq(user.booking.depositpaid)
                    }
                    if (expect(arr.bookingdates.checkin).not.to.be.null) {
                        expect(arr.bookingdates.checkin).to.eq(user.booking.bookingdates.checkin)
                    }
                    if (expect(arr.bookingdates.checkout).not.to.be.null) {
                        expect(arr.bookingdates.checkout).to.eq(user.booking.bookingdates.checkout)
                    }
                    if (expect(arr.additionalneeds).not.to.be.null) {
                        expect(arr.additionalneeds).to.eq(user.booking.additionalneeds)
                    }
                }
                })
        })
    })
    it('Verify the Created Booking ID with firstname and lastname', () => {
        cy.fixture('User.json').then((user) => {

            let Bookingurl = `https://restful-booker.herokuapp.com/booking?firstname=${user.booking.firstname}&lastname=${user.booking.lastname}`
            cy.request({
                method: 'GET',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                }
            }).then((response) => {
                if (response.status == 200) {
                    cy.log(JSON.stringify(response.body))
                    let arr = response.body;
                    cy.log(arr);


                }
            })
        })
    })
    it('Verify the Created Booking ID with Checkin and Checkout', () => {
        cy.fixture('User.json').then((user) => {
            let checkin= user.booking.bookingdates.checkin;
            let checkout=user.booking.bookingdates.checkout;
            let Bookingurl = `https://restful-booker.herokuapp.com/booking?checkin=${checkin}&checkout=${checkout}`
            cy.request({
                method: 'GET',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                }
            }).then((response) => {
                if (response.status == 200) {
                    cy.log(JSON.stringify(response.body))
                    let arr = response.body;
                    cy.log(arr);

                }
            })
        })
    })
})