describe('API Automation | Verify the Booking with different filter', () => {
    it('Verify the user ID without any filter', () => {
        cy.request({
            method: 'GET',
            url: 'https://restful-booker.herokuapp.com/booking',
            headers: {
                "Content-Type": "application/json",
            }, 
            body:{
                username: "admin",
                password: "password123"
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                let arr = response.body;
                if(arr.length >0){
                expect(arr[0].bookingid).to.not.be.null
                cy.readFile('cypress/fixtures/User.json').then((user)=> {
                    user.id =arr[0].bookingid
                cy.writeFile('cypress/fixtures/User.json', user) 
                })
                }
            }
        })  
    })
    it('Verify with Booking ID & get firstname and lastname', () => {
        cy.fixture('User.json').then((user)=> {
            cy.log(user.id)
            let Bookingurl = `https://restful-booker.herokuapp.com/booking/${user.id}`
        cy.request({
            method: 'GET',
            url: Bookingurl,
            headers: {
                "Content-Type": "application/json",
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                let arr = response.body;
                cy.log(arr);
                cy.readFile('cypress/fixtures/User.json').then((user)=> {
                    user.firstname =arr.firstname
                    user.lastname =arr.lastname
                    user.bookingdates =arr.bookingdates
                cy.writeFile('cypress/fixtures/User.json', user) 
                })
                
            }
        }) 
    }) 
    })
    it('Verify the Booking ID with firstname and lastname', () => {
        cy.fixture('User.json').then((user)=> {
            
            let Bookingurl = `https://restful-booker.herokuapp.com/booking?firstname=${user.firstname}&lastname=${user.lastname}`
        cy.request({
            method: 'GET',
            url: Bookingurl,
            headers: {
                "Content-Type": "application/json",
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                let arr = response.body;
                cy.log(arr);
                
            }
        }) 
    }) 
    })
    it('Verify the Booking ID with Checkin and Checkout', () => {
        cy.fixture('User.json').then((user)=> {
            
            let Bookingurl = `https://restful-booker.herokuapp.com/booking?checkin=${user.bookingdates.checkin}&checkout=${user.bookingdates.checkout}`
        cy.request({
            method: 'GET',
            url: Bookingurl,
            headers: {
                "Content-Type": "application/json",
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                let arr = response.body;
                cy.log(arr);
                
            }
        }) 
    }) 
    })
})
