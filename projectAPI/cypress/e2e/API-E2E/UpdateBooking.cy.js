describe('API Automation | Updation in the Booking record of User', () => {
    before(() => {
        cy.request({
            method: 'POST',
            url: 'https://restful-booker.herokuapp.com/auth',
            headers: {
                "Content-Type": "application/json",
            }, 
            body:{
                username: "admin",
                password: "password123"
            }
        }).then((response) => {
            if (response.status == 200) {
                cy.log(JSON.stringify(response.body))
                expect(response.body.token).to.not.be.null
                cy.writeFile('cypress/fixtures/token.json', { token:  response.body.token})  
            }else{
                cy.writeFile('cypress/fixtures/token.json', { token: "" })
            }
        })  
    })
    it('Verify the Updation of totalPrice new Booking', () => {
        cy.fixture('token.json').then((Admin) => {
        cy.readFile('cypress/fixtures/User.json').then((user) => {
            let randomNum = Math.floor((Math.random() * 10000) + 1);
            let updateRecord ={
                "bookingid": user.bookingid,
                "booking": {
                "firstname": user.booking.firstname,
                "lastname": user.booking.lastname,
                "totalprice": randomNum,
                "depositpaid": user.booking.depositpaid,
                "bookingdates": user.booking.bookingdates,
                "additionalneeds": user.booking.additionalneeds
            }
        }
            let token = Admin.token;
            let Bookingurl = `https://restful-booker.herokuapp.com/booking/${user.bookingid}`
            //update the totalPrice value in record
           
            cy.request({
                method: 'PUT',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                    "Cookie" : `token=${token}`
                },
                body: updateRecord.booking
            }).then((response) => {
                if (response.status == 200) {
                    cy.log(JSON.stringify(response.body))
                    let arr = response.body;
                    cy.writeFile('cypress/fixtures/User.json',updateRecord)
                }
            })
        })
    })
    })
    it('Verify the Updated User with Booking ID & assert all data along with updated one', () => {
        cy.readFile('cypress/fixtures/User.json').then((user) => {
            cy.log(user.id)
            let Bookingurl = `https://restful-booker.herokuapp.com/booking/${user.bookingid}`
            cy.request({
                method: 'GET',
                url: Bookingurl,
                headers: {
                    "Content-Type": "application/json",
                }
            }).then((response) => {
                if (response.status == 200) {
                    let arr = response.body;
                    if (expect(arr.firstname).not.to.be.null) {
                        expect(arr.firstname).to.eq(user.booking.firstname)
                    }
                    if (expect(arr.lastname).not.to.be.null) {
                        expect(arr.lastname).to.eq(user.booking.lastname)
                    }
                    if (expect(arr.totalprice).not.to.be.null) {
                        expect(arr.totalprice).to.eq(user.booking.totalprice)
                    }
                    if (expect(arr.depositpaid).not.to.be.null) {
                        expect(arr.depositpaid).to.eq(user.booking.depositpaid)
                    }
                    if (expect(arr.bookingdates.checkin).not.to.be.null) {
                        expect(arr.bookingdates.checkin).to.eq(user.booking.bookingdates.checkin)
                    }
                    if (expect(arr.bookingdates.checkout).not.to.be.null) {
                        expect(arr.bookingdates.checkout).to.eq(user.booking.bookingdates.checkout)
                    }
                    if (expect(arr.additionalneeds).not.to.be.null) {
                        expect(arr.additionalneeds).to.eq(user.booking.additionalneeds)
                    }
                }
                })
        })
    })
})